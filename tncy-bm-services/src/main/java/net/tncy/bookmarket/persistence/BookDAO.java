package net.tncy.bookmarket.persistence;

import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import net.tncy.bookmarket.data.Book;

/**
 *
 * @author xavie
 */
public class BookDAO extends AbstractDAO<Book> {

    public BookDAO(EntityManager em) {
        super(em, Book.class);
    }

    public List<Book> findByTitle(String title) {
        List<Book> result = em.createQuery("select e from " + entityClass.getName() + " e where title like " + title).getResultList();
        return result;
    }

    public Book findByIsbn(String isbn) {
        Query query = em.createNamedQuery(Book.FIND_BY_ISBN, entityClass);
        return (Book) query.getSingleResult();
    }
    
}
