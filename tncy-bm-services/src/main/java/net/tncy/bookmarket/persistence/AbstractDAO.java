package net.tncy.bookmarket.persistence;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;

public abstract class AbstractDAO<T extends Serializable> {

    protected Class<T> entityClass;

    protected EntityManager em;

    protected Logger logger = Logger.getLogger(getClass().getName());

    protected AbstractDAO(EntityManager em, Class entityClass) {
        this.em = em;
        this.entityClass = entityClass;
        //ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        //this.entityClass = (Class) genericSuperclass.getActualTypeArguments()[1];
    }

    public T findOneById(Object id) {
        logger.log(Level.INFO, ">>> findOneById: {0}", id);
        return em.find(entityClass, id);
    }

    public List< T> findAll() {
        Query query = em.createQuery("select e from " + entityClass.getName() + " e");
        return query.getResultList();
    }
    
    public long countAll() {
        Query query = em.createQuery("select count(e) from " + entityClass.getName() + " e");
        return (Long)query.getSingleResult();
    }

    public void create(T entity) {
        em.persist(entity);
        
    }

    public T update(T entity) {
        return em.merge(entity);
    }

    public void delete(T entity) {
        em.remove(entity);
    }

    public void deleteById(Object entityId) {
        T entity = findOneById(entityId);
        delete(entity);
    }
}
