package net.tncy.bookmarket.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public class Hello {

    @GET
    @Path("/json")
    @Produces({"application/json"})
    public String getHelloWorldJSON() {
        return "{\"result\":\"Hello World!\"}";
    }

    @GET
    @Path("/xml")
    @Produces({"application/xml"})
    public String getHelloWorldXML() {
        return "<?xml version=\"1.0\"?>" + "<result>Hello world!</result>";
    }

    @GET
    @Path("/plain")
    @Produces(MediaType.TEXT_PLAIN)
    public String getHelloWorldPlainText() {
        return "Hello World!";
    }

    @GET
    @Path("/html")
    @Produces(MediaType.TEXT_HTML)
    public String getHelloWorldHTML() {
        return "<html> " + "<title>" + "Hello World!" + "</title>"
                + "<body><h1>" + "Hello World!" + "</body></h1>" + "</html> ";
    }
}
