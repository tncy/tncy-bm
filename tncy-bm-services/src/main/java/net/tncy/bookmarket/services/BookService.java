package net.tncy.bookmarket.services;

import net.tncy.bookmarket.data.Book;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.naming.NamingException;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.PersistenceContext;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import net.tncy.bookmarket.persistence.BookDAO;

@Stateless
@Path("/book")
@Produces("application/json")
public class BookService {

    private final static String PERSISTENCE_UNIT_NAME = "BOOKMARKET-PU";

    private static Logger LOGGER = Logger.getLogger(BookService.class.getName());

    @Context
    private UriInfo uriInfo;

    @PersistenceContext(unitName = PERSISTENCE_UNIT_NAME)
    private EntityManager em;

    @GET
    @Path("/list")
    public List<Book> getAll() {
        BookDAO dao = new BookDAO(em);
        List<Book> result = dao.findAll();
        LOGGER.log(Level.INFO, ">>> Found {0} books", result.size());
        return result;
    }

    @GET
    @Path("/count")
    public long count() {
        BookDAO dao = new BookDAO(em);
        return dao.countAll();
    }

    @GET
    @Path("/{id: \\d+}")
    public Book getBookById(@PathParam("id") int id) {
        LOGGER.log(Level.INFO, ">>> Looking for book with id {0}", id);
        BookDAO dao = new BookDAO(em);
        Book result = dao.findOneById(id);
        LOGGER.log(Level.INFO, ">>> Found book {0}", result);
        return result;
    }

    @POST
    public Book create(Book book) {
        LOGGER.log(Level.INFO, ">>> New book");
        BookDAO dao = new BookDAO(em);
        dao.create(book);
        LOGGER.log(Level.INFO, ">>> Created book {0}", book);
        return book;
    }

    @PUT
    public Book update(Book book) {
        BookDAO dao = new BookDAO(em);
        dao.update(book);
        return book;
    }

    @DELETE
    @Path("/{id: \\d+}")
    public void delete(@PathParam("id") int id) {
        BookDAO dao = new BookDAO(em);
        Book book = dao.findOneById(id);
        dao.delete(book);
    }
}
