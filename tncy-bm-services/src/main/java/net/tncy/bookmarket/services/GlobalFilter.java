package net.tncy.bookmarket.services;

import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;

/* Un filtre général (grace à @PreMatching) qui va s'appliquer à tous les 
 * services JAX-RS déployés sur le conteneur.
 */
@Provider
@PreMatching
public class GlobalFilter implements ContainerRequestFilter, ContainerResponseFilter {

    private static Logger logger = Logger.getLogger(GlobalFilter.class.getName());
    
    private final static String REQUEST_PATTERN = ">> {0} {1}";
    
    private final static String RESPONSE_PATTERN = "<< {0} {1}";

    @Override
    public void filter(ContainerRequestContext requestContext) {
        String message = MessageFormat.format(REQUEST_PATTERN, requestContext.getMethod(), requestContext.getUriInfo().getAbsolutePath());
        logger.log(Level.WARNING, message);
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        String message = MessageFormat.format(RESPONSE_PATTERN, responseContext.getStatus());
        logger.log(Level.WARNING, message);
    }

}
