/* Populates BOOK table */
INSERT INTO BOOKS(TITLE, AUTHOR, PUBLISHER, PUBLICATIONDATE, ISBN) values ('Clean Code: A Handbook of Agile Software Craftsmanship', 'Robert C. Martin', 'Pearson', '2008-08-01', '978-0132350884')

INSERT INTO BOOKS(TITLE, AUTHOR, PUBLISHER, PUBLICATIONDATE, ISBN) values ('Extreme Programming Explained: Embrace Change', 'Kent Beck', 'Addison Wesley', '2004-11-16', '978-0321278654')

INSERT INTO BOOKS(TITLE, AUTHOR, PUBLISHER, PUBLICATIONDATE, ISBN) values ('Design Patterns: Elements of Reusable Object-Oriented Software', 'Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides', 'Addison Wesley', '1980-01-17', '978-0201633610')

INSERT INTO BOOKS(TITLE, AUTHOR, PUBLISHER, PUBLICATIONDATE, ISBN) values ('Scrum : Le guide pratique de la méthode agile la plus populaire', 'Claude Aubry', 'Dunod', '2011-09-07', '978-2100563203')

INSERT INTO BOOKS(TITLE, AUTHOR, PUBLISHER, PUBLICATIONDATE, ISBN) values ('Apache Maven: Version 2 et 3', 'Nicolas De loof, Arnaud Héritier', 'Pearson', '2011-08-25', '978-2744024948')

INSERT INTO BOOKS(TITLE, AUTHOR, PUBLISHER, PUBLICATIONDATE, ISBN) values ('Java EE 5', 'Antonio Goncalves', 'Pearson', '2007-05-25', '978-2212120387')