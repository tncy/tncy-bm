package net.tncy.bookmarket.data;

/**
 *
 * @author Xavier Roy (xavier.roy@telecomnancy.net)
 */
public class InventoryEntry {
    
    private Book book;
    
    private int quantity;
    
    private float averagePrice;
    
    private float price;

    public InventoryEntry(Book book, int quantity, float averagePrice, float price) {
        this.book = book;
        this.quantity = quantity;
        this.averagePrice = averagePrice;
        this.price = price;
    }
    
    public void addBookLot(int lotSize, float unitPrice) {
        this.averagePrice = (this.quantity * this.averagePrice + lotSize * unitPrice) / (this.quantity + lotSize);
        this.quantity += lotSize;
    }
    
    public void removeBookLot(int lotSize) {
        this.quantity -= lotSize;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(float averagePrice) {
        this.averagePrice = averagePrice;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
    
    
}
