package net.tncy.bookmarket.data;

import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
// import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Xavier Roy (xavier.roy@telecomnancy.net)
 */
@Entity
@Table(
        name = "BOOKSTORES",
        uniqueConstraints = {
            @UniqueConstraint(name = "BOOKSTORE_UNIQUE_NAME", columnNames = "NAME")
        }
)
@NamedQueries({
    @NamedQuery(name = Bookstore.FIND_ALL, query = "SELECT bs FROM BOOKSTORE bs"),
    @NamedQuery(name = Bookstore.FIND_BY_NAME, query = "SELECT bs FROM BOOKSTORE bs WHERE bs.name LIKE :name")
})
//@XmlRootElement
public class Bookstore {

    public static final String FIND_ALL = "Bookstore.findAll";

    public static final String FIND_BY_NAME = "Bookstore.findByName";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private String name;

    private List<InventoryEntry> inventory;

    public Bookstore() {
        this.inventory = new ArrayList<InventoryEntry>();
    }

    public Bookstore(int id, String name) {
        this();
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<InventoryEntry> getInventory() {
        return inventory;
    }
    
    public void addBookLot(Book book, int lotSize, float unitPrice) {
        InventoryEntry entry = inventoryEntryLookup(book.getId());
        if (entry != null) {
            entry.addBookLot(lotSize, unitPrice);
        } else {
            entry = new InventoryEntry(book, lotSize, unitPrice, unitPrice);
            inventory.add(entry);
        }
    }
    
    public InventoryEntry inventoryEntryLookup(Integer bookId) {
        // TODO
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
    

}
