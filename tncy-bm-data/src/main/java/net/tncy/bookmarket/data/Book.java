package net.tncy.bookmarket.data;

import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import net.tncy.validator.constraints.books.ISBN;

/**
 *
 * @author Xavier Roy (xavier.roy@telecomnancy.net)
 */
@Entity
@Table(
        name = "BOOKS",
        uniqueConstraints = {
            @UniqueConstraint(name = "BOOK_UNIQUE_ISBN", columnNames = "ISBN")
        }
)
@NamedQueries({
    @NamedQuery(name = Book.FIND_ALL, query = "SELECT b FROM Book b"),
    @NamedQuery(name = Book.FIND_BY_ISBN, query = "SELECT b FROM Book b WHERE b.isbn = :isbn")
})
public class Book implements Serializable {

    public static final String FIND_ALL = "Book.findAll";

    public static final String FIND_BY_ISBN = "Book.findByISBN";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    @NotNull
    private String title;

    private String author;

    private String publisher;

    @NotBlank
    @NotNull
    @ISBN
    @Column(name = "ISBN")
    private String isbn;

    @Temporal(TemporalType.DATE)
    private Date publicationDate;

    public Book() {
    }

    public Book(String title, String author, String publisher, Date publicationDate, String isbn) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.publicationDate = publicationDate;
        this.isbn = isbn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getIsbn() {
        return isbn;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

}
